------------------------------------------------------------------------------
-- |
-- Copyright: (c) 2021 Gerry Agbobada
-- License: MIT-style (see LICENSE)
--
-- Maintainer: git@gagbo.net
-- Stability: unstable
-- Portability: portable
-- Created: Tue Sep 21, 2021 21:03
------------------------------------------------------------------------------

import Xmobar
import Gagbo.Config.LaptopA

main :: IO ()
main = xmobar $ bottomConfig "gag"
