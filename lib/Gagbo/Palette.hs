-- | Spin-off of jaor xmobar Config module
module Gagbo.Palette
  ( Palette (..),
    baseConfig,
    (<~>),
    (>~<),
    mkArgs,
    defaultHeight,
    fc,
    fn,
    fni,
    gorgeHoussePalette,
  )
where

import Xmobar

defaultHeight :: Int
defaultHeight = 24

defaultIcons :: String
defaultIcons = "/home/gagbo/.config/xmobar/data/xpm"

data Palette = Palette
  { pNormal :: String,
    pLow :: String,
    pHigh :: String,
    pDim :: String,
    pGreen :: String,
    pRed :: String,
    pBlue :: String,
    pYellow :: String,
    pCyan :: String,
    pMagenta :: String,
    pFont :: String,
    pBorder :: String,
    pForeground :: String,
    pBackground :: String,
    pAlpha :: Int,
    pIconRoot :: String
  }

fc color thing = "<fc=" ++ color ++ ">" ++ thing ++ "</fc>"

fn n thing = "<fn=" ++ show n ++ ">" ++ thing ++ "</fn>"

-- Won't work until there are many more fonts in the baseConfig
fni = fn 6

icons name = "/home/" ++ name ++ "/.config/xmobar/data/xpm"

gorgeHoussePalette :: String -> Palette
gorgeHoussePalette username =
  Palette
    { pNormal = "#CEDBE5",
      pLow = "#899299",
      pHigh = "#E55C50",
      pDim = "#959EA5",
      pGreen = "#B2FF66",
      pRed = "#FF7266",
      pBlue = "#66A5FF",
      pYellow = "#FFDF66",
      pCyan = "#66FFD8",
      pMagenta = "#9866FF",
      pFont = "xft:KoHo:size=12:medium:antialias=true",
      pBorder = "#0C0C14",
      pForeground = "#CEDBE5",
      pBackground = "#0C0C14",
      pAlpha = 229,
      pIconRoot = icons username
    }

baseConfig :: Palette -> Config
baseConfig p =
  defaultConfig
    { font = pFont p,
      borderColor = pBorder p,
      fgColor = pForeground p,
      bgColor = pBackground p,
      -- Choose a monospace font as the 1st one of this array
      additionalFonts = ["xft:Victor Mono:pixelsize=13"],
      border = NoBorder,
      alpha = pAlpha p,
      -- Both lowerOnStart and overrideRedirect are True because
      -- xmonad handles the geometry properly with avoidStruts
      overrideRedirect = True,
      lowerOnStart = True,
      hideOnStart = False,
      allDesktops = True,
      persistent = True,
      sepChar = "%",
      alignSep = "}{",
      iconRoot = pIconRoot p
    }

(<~>) :: Palette -> [String] -> [String]
(<~>) p args =
  args ++ ["--low", pLow p, "--normal", pNormal p, "--high", pHigh p]

(>~<) :: Palette -> [String] -> [String]
(>~<) p args =
  args ++ ["--low", pHigh p, "--normal", pNormal p, "--high", pLow p]

mkArgs :: Palette -> [String] -> [String] -> [String]
mkArgs p args extra = concat [p <~> args, ["--"], extra]
