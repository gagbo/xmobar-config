-- | Spin-off of jaor configuration
module Gagbo.Monitors where

import Gagbo.Palette
import Xmobar

memoryBar = Memory ["-t", "<icon=memory-icon_20.xpm/> Mem <fn=1><usedbar> (<usedratio>%)</fn>", "-p", "3"] 10

cpuBar p =
  Cpu
    ( p
        <~> [ "-t",
              "<icon=cpu_20.xpm/> " ++ fn 1 "<bar> (<total>%)",
              "-L",
              "3",
              "-H",
              "50",
              "-p",
              "3"
            ]
    )
    10

parisTime = DateZone "%a %d %b %H:%M:%S" "fr_FR.UTF-8" "Europe/Paris" "parisTime" 10

dynNetwork p =
  DynNetwork
    ( p
        <~> [ "-L",
              "8",
              "-H",
              "32",
              "-m",
              "4",
              "-t",
              "<dev> <icon=net_down_20.xpm/>" ++ fn 1 "<rx>KB" ++ " <icon=net_up_20.xpm/>" ++ fn 1 "<tx>KB"
            ]
    )
    10

masterVolume p =
  Volume
    "default"
    "Master"
    [ "-f",
      "▮",
      "-b",
      "▯",
      "-t",
      fc (pDim p) "Vol:" ++ "<status>" ++ fc (pDim p) (fn 1 "<volume>%"),
      "--",
      "-C",
      pGreen p,
      "-c",
      pRed p
    ]
    10

masterAlsa p =
  Alsa
    "default"
    "Master"
    [ "-f",
      "▮",
      "-b",
      "▯",
      "-t",
      fc (pDim p) "Vol:" ++ "<status>" ++ fc (pDim p) (fn 1 "<volume>%"),
      "--",
      "-C",
      pGreen p,
      "-c",
      pRed p
    ]

pcmAlsa p =
  Alsa
    "default"
    "PCM"
    [ "-f",
      "▮",
      "-b",
      "▯",
      "-t",
      fc (pDim p) "Vol:" ++ "<status>" ++ fc (pDim p) (fn 1 "<volume>%"),
      "--",
      "-C",
      pGreen p,
      "-c",
      pRed p
    ]

thinkTemp p =
  MultiCoreTemp
    ( mkArgs
        p
        ["-t", "<core1>°C", "-L", "40", "-H", "70", "-d", "0"]
        []
    )
    50

avgCoreTemp p =
  MultiCoreTemp
    ( p
        <~> [ "-t",
              "<avg>°",
              "-L",
              "50",
              "-H",
              "75",
              "-d",
              "0"
            ]
    )
    50

coreTemp p =
  MultiCoreTemp
    ( p
        <~> [ "-t",
              "<max>°",
              "-L",
              "50",
              "-H",
              "75",
              "-d",
              "0"
            ]
    )
    50

topProc p =
   TopProc
    ( p
        <~> [ "-t",
              "<both1>",
              "-L",
              "10",
              "-H",
              "70",
              "-d",
              "0",
              "-M",
              "40"
            ]
    )
    50

topMem p =
   TopMem
    ( p
        <~> [ "-t",
              "<both1>",
              "-L",
              "10",
              "-H",
              "70",
              "-d",
              "0",
              "-M",
              "40"
            ]
    )
    50
