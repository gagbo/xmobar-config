-- |
module Gagbo.Config.LaptopA (topConfig, bottomConfig) where

import Gagbo.Monitors
import Gagbo.Palette
import Xmobar

top p =
  (baseConfig p)
    { position = Top,
      template = " <icon=haskell_20.xpm/> " ++ fc (pDim p) "|" ++ " %XMonadLog% }{ " ++ fc (pBlue p) "<icon=calendar-clock-icon_20.xpm/> %parisTime%" ++ " | %multicoretemp% " ++ fc (pDim p) "%cpu% | %memory%",
      commands =
        [ Run $ XMonadLog,
          Run $ cpuBar p,
          Run $ memoryBar,
          Run $ parisTime,
          Run $ coreTemp p
        ]
    }

bot p =
  (baseConfig p)
    { position = BottomW L 90, -- Leaving space for trayer
      template = "%kbd%  %dynnetwork% } %default:Master% { %top% %topmem%",
      commands =
        [ Run $ dynNetwork p,
          Run $ masterVolume p,
          Run $ topMem p,
          Run $ topProc p,
          Run $ Kbd [("us(altgr-intl)", "US"), ("fr(bepo)", "FR")]
        ]
    }

topConfig :: String -> Config
topConfig = top . gorgeHoussePalette

bottomConfig :: String -> Config
bottomConfig = bot . gorgeHoussePalette
