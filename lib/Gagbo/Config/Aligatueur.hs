-- |
module Gagbo.Config.Aligatueur (aligatueurConfig) where

import Gagbo.Monitors
import Gagbo.Palette
import Xmobar

config p =
  (baseConfig p)
    { position = TopW L 95, -- Leaving space for trayer
      template = " <icon=haskell_20.xpm/> " ++ fc (pDim p) "|" ++ " %XMonadLog% } " ++ fc (pBlue p) "<icon=calendar-clock-icon_20.xpm/> %parisTime%" ++ " { %alsa:default:Master% | %dynnetwork% | " ++ fc (pDim p) "%cpu%" ++ " | " ++ fc (pDim p) "%memory%",
      commands =
        [ Run $ cpuBar p,
          Run $ memoryBar,
          Run $ parisTime,
          Run $ dynNetwork p,
          Run $ XMonadLog,
          Run $ masterVolume p,
          Run $ masterAlsa p
        ]
    }

aligatueurConfig :: String -> Config
aligatueurConfig = config . gorgeHoussePalette
